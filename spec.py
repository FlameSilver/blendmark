import platform, os, re
from subprocess import check_output
from pciids import pci

uName = platform.uname()
print(uName, "\n")

def hardware(winCom): #Fetches the output of a Windows Command, parses/cleans the raw output and sorts it into Dictionaries
    unit = check_output(winCom).decode().split("\r\r\n") #Fetches Windows Command and splits it into a list
    tempList = filter(lambda a: a, map(str.strip, unit)) #Strips the Values of the List of their white space
    temp = [[y.strip() for y in x.split("  ") if y != ""] for x in tempList]

    t = {}
    for i in range(len(temp) - 1): #Splits the info into a Dictionary based on the input, then returns the Dictionary
        if "path" in winCom:
            t["GPU" + str(i)] = {temp[0][0]:temp[i+1][0], temp[0][1]:temp[i+1][1], temp[0][2]:temp[i+1][2]}
        elif "MEMORYCHIP" in winCom:
            t["DIMM" + str(i+1)] = {temp[0][0]:temp[i+1][0], temp[0][1]:temp[i+1][1]}
        else:
            t[temp[i+1][0]] = temp[i+1][1], temp[i+1][2]
    return t


def DevID(idList): #Sorts GPU Device IDs into a Dectionay
    gpuDict = {"Vendor":"", "Devices":"", "Subsystems":"", "Rev":""}

    for i, id in zip(gpuDict, idList):
        x = id.split("_")
        y = x[1].lower()
        if len(y)==8:
            z = y[4:8] + ":" + y[0:4]
            gpuDict[i] = z
            continue
        gpuDict[i] = y
    return gpuDict


def puNix(nixCom): #Fetches the output of a Linux command, parses and returns neccesary information
    ls = os.popen(nixCom).read()
    list = ls.split("\n")
    ls = [[y.strip() for y in x.split(":") if y != ""] for x in list]
    for value in ls:
        if value[0] == "Model name" and nixCom == "lscpu": #Returns CPU Model Name
            return {value[0]:value[1].replace("   ", "").replace("  ", " ")}

        elif value[0] == "MemTotal" and nixCom == "cat /proc/meminfo": #Returns Total System Memory
            return {value[0]:value[1]}

        elif value[0] == "Vendor" and nixCom == "lspci -mnnvd ::0300": #Stores GPU Vendor
            venName = value[1].split(" ")[0]
        elif value[0] == "SDevice" and nixCom == "lspci -mnnvd ::0300": #Returns GPU Vendor + GPU Model Name
            return {value[0]:venName + " " + value[1][:-7]}


if uName[0] == 'Windows':
    import winreg


    cpu = hardware("wmic cpu get name, deviceid, NumberOfCores")
    print(cpu)

    gpu = hardware("wmic path win32_VideoController get name, adapterram, pnpdeviceid")
    print(gpu)

    mem = hardware("wmic MEMORYCHIP get Capacity, Speed")
    print(mem, "\n")

    gpuID = DevID(gpu["GPU0"]["PNPDeviceID"].split("&", 3))

    try: #Try's to fetch GPU Model Name from Subsystem ID
        gpuName = pci.vendors[gpuID["Vendor"]].devices[gpuID["Devices"]].subdevices[gpuID["Subsystems"]].name
    except KeyError: #if Subsystem ID doesn't have a value, fetch value of Device ID instead
        gpuName = pci.vendors[gpuID["Vendor"]].devices[gpuID["Devices"]].name

    #Renames the GPU name reported by Windows to the correct GPU Model Name
    gpu["GPU0"]["Name"] = gpu["GPU0"]["Name"].split(" ")[0] + " " + gpuName
    print(gpu)


elif uName[0] == 'Linux':
    cpu = puNix("lscpu")
    print(cpu)

    gpu = puNix("lspci -mnnvd ::0300")
    print(gpu)

    mem = puNix("cat /proc/meminfo")
    print(mem)


else:
    print("\nshit")
