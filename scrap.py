import requests
from bs4 import BeautifulSoup
import re
from test import *

intel = requests.get("https://ark.intel.com/Search/FeatureFilter?productType=processors&FamilyText=")
amd = requests.get("https://www.amd.com/en/products/specifications/processors/")
html = BeautifulSoup(amd.text, "html.parser")
htmli = BeautifulSoup(intel.text, "html.parser")

tbody = html.find("tbody")
thead = html.find("thead")


for tr in tbody.find_all("tr"):
    for td, th in zip(tr.find_all("td"), thead.find_all("th")):
        if re.sub("™", "", tr.find_all("td")[1].text) in cpu["CPU0"] and td.text != "":
            print(th.text.strip(), ":", td.text.strip())
    #print("\n")
